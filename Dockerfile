FROM daptin/daptin:travis
COPY schema_devsupport_daptin.yaml /opt/daptin/schema_devsupport_daptin.yaml
COPY schema_devsupport_feedback_daptin.json /opt/daptin/schema_devsupport_feedback_daptin.json

EXPOSE 5000
EXPOSE 5003
ENTRYPOINT ["/opt/daptin/daptin", "-runtime", "release", "-port", ":5000", "-db_type", "postgres",  "-db_connection_string", "host=staging-postgres port=5432 user=devsupport password=devsupportpassword dbname=devsupport sslmode=disable"]
